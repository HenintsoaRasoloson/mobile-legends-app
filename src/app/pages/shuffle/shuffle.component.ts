import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PlayerService } from 'src/app/shared/services/player.service';

@Component({
  selector: 'app-shuffle',
  templateUrl: './shuffle.component.html',
  styleUrls: ['./shuffle.component.css'],
})
export class ShuffleComponent {
  notHome = true;
  UserList = [];
  playerFormGroup = new FormGroup({
    pseudo: new FormControl(null, Validators.required),
  });
  leftTeam = [];
  rightTeam = [];
  shuffledTeam: boolean = false;

  constructor(private playerService: PlayerService) {}

  handleAdd() {
    this.playerFormGroup.markAllAsTouched();
    const userPseudo = this.playerFormGroup.get('pseudo').value;
    if (userPseudo == null || userPseudo == '') return;
    if (this.UserList.length >= 10) return;
    this.UserList.push({
      id: new Date().getTime(),
      name: userPseudo,
    });
    this.playerFormGroup.get('pseudo').setValue('');
  }

  handleShuffle() {
    this.shuffledTeam = true;
    const res = this.playerService.shuffle([...this.UserList]);
    this.leftTeam = res.left;
    this.rightTeam = res.right;
  }
}
