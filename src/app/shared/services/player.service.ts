import { Injectable } from '@angular/core';
import { IPlayer } from '../interfaces/IPlayer';

@Injectable({
  providedIn: 'root',
})
export class PlayerService {
  constructor() {}

  shuffle(a: IPlayer[]) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = a[i];
      a[i] = a[j];
      a[j] = x;
    }
    return {
      left: a.splice(0, a.length / 2),
      right: a,
    };
  }
}
