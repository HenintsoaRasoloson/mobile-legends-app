import { Component, Input } from '@angular/core';
import { IPlayer } from '../../interfaces/IPlayer';

@Component({
  selector: 'app-player-card',
  templateUrl: './player-card.component.html',
  styleUrls: ['./player-card.component.css'],
})
export class PlayerCardComponent {
  @Input() title = '';
  @Input() playerList!: IPlayer[];

  poste = [
    {
      name: 'Jungler',
      icon: 'assets/img/jungler.webp',
    },
    {
      name: 'Mid Lane',
      icon: 'assets/img/mid.jpg',
    },
    {
      name: 'Roamer',
      icon: 'assets/img/roam.jpg',
    },
    {
      name: 'Gold Lane',
      icon: 'assets/img/gold.jpg',
    },
    {
      name: 'Exp Lane',
      icon: 'assets/img/exp.jpg',
    },
  ];
}
