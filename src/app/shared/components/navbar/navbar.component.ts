import { Component, Input } from '@angular/core';
import { HostListener } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent {
  scrolled = false;
  currentPath = '';
  additionnalClass = '';

  constructor(private router: Router) {
    this.currentPath = router.url;
    if (this.currentPath != '/') {
      this.additionnalClass = 'nothome';
    } else {
      this.additionnalClass = '';
    }
  }

  @HostListener('window:scroll', ['$event'])
  onScroll(event: Event) {
    if (window.scrollY > 500) {
      this.scrolled = true;
    } else {
      this.scrolled = false;
    }
  }
}
